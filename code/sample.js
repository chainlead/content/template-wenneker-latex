import Portis from '@portis/web3';
import Web3 from 'web3';

const node = {
	nodeUrl: 'https://localhost:4300',
	chainId: 99,
};

const portis = new Portis(<portis_dapp_id>, 
                           node);
const web3 = new Web3(portis.provider);
